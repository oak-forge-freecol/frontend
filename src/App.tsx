/* eslint-disable object-curly-newline */
import React from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Theme from "./Containers/Theme/Theme";
import WelcomePage from "./Components/WelcomePage/WelcomePage";
import AuthPage from "./Components/AuthPage/AuthPage";
import HomePage from "./Components/HomePage/HomePage";

const App: React.FC = () => (
  <Theme>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={WelcomePage} />
        <Route path="/login" component={AuthPage} />
        <Route path="/register" component={AuthPage} />
        <Route path="/home" component={HomePage} />
        <Route path="*" component={WelcomePage}>
          <Redirect to="/login" />
        </Route>
      </Switch>
    </BrowserRouter>
  </Theme>
);

export default App;
