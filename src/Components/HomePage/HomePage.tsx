import React from "react";
import { Box } from "@material-ui/core";
import { useStyles } from "./HomePageStyles";
import PrivatePageContainer from "../../Containers/PrivatePageContainer/PrivatePageContainer";

const HomePage: React.FC = () => {
  const styles = useStyles();
  return (
    <PrivatePageContainer>
      <Box>
        <div className={styles.infoWrapper} />
      </Box>
    </PrivatePageContainer>
  );
};

export default HomePage;
