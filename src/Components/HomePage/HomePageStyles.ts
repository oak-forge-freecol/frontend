import { makeStyles } from "@material-ui/core/styles";
import generalPaper from "../../assets/oakForge-freecol/general-paper.png";
import soliderPaper from "../../assets/oakForge-freecol/solider-paper.png";

export const useStyles = makeStyles((theme) => ({
  infoWrapper: {
    width: "300px",
    minHeight: "600px",
    marginTop: "20px",
    backgroundImage: `url(${soliderPaper})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top",
    backgroundSize: "contain",
    [theme.breakpoints.up("sm")]: {
      backgroundImage: `url(${generalPaper})`,
      width: "550px",
    },
  },
}));
