import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  paragraph: {
    fontSize: "12px",
    margin: "10px 0",
  },
  buttonsWrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    gap: "10px",
    "&> a:nth-child(2)": {
      [theme.breakpoints.up("sm")]: {
        display: "none",
      },
    },
  },
}));
