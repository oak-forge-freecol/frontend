import React from "react";
import { Typography, Box, useMediaQuery } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { useStyles } from "./WelcomePageStyles";
import PublicContentCard from "../../Containers/PublicContentCard/PublicContentCard";
import MainButton from "../../Shared/MainButton/MainButton";
import PublicPageContainer from "../../Containers/PublicPageContainer/PublicPageContainer";

const WelcomePage: React.FC = () => {
  const theme = useTheme();
  const styles = useStyles(theme);
  const matches = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <PublicPageContainer>
      <PublicContentCard>
        <Typography align="center" className={styles.paragraph}>
          FreeCol is a turn-based strategy game based on the old game
          Colonization, and similar to Civilization. The objective of the game
          is to create an independent nation. You start with only a few
          colonists defying the stormy seas in their search for new land. Will
          you guide them on the Colonization of a New World?
        </Typography>
        <Box className={styles.buttonsWrapper}>
          <MainButton btnHandler="/register" wide>
            Register
          </MainButton>
          {matches && <MainButton btnHandler="/login">Login</MainButton>}
        </Box>
      </PublicContentCard>
    </PublicPageContainer>
  );
};

export default WelcomePage;
