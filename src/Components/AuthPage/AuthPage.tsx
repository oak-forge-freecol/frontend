/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import PublicContentCard from "../../Containers/PublicContentCard/PublicContentCard";
import PublicPageContainer from "../../Containers/PublicPageContainer/PublicPageContainer";
import LoginForm from "./LoginForm/LoginForm";
import RegistrationForm from "./RegistrationForm/RegistrationForm";

interface FormState {
  email: string;
  password: string;
  password2?: string;
}

const AuthPage: React.FC = () => {
  const location = useLocation();
  const path = location.pathname;

  const [value, setValue] = useState<FormState>({
    email: "",
    password: "",
    password2: "",
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    setValue((prevState) => ({
      ...prevState,
      [target.name]: target.value,
    }));
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (path === "/login" && value.email && value.password) {
      console.log(value.email, value.password);
    } else if (
      path === "/register" &&
      value.email &&
      value.password &&
      value.password2
    ) {
      console.log(value.email, value.password);
    }
  };

  return (
    <PublicPageContainer>
      <PublicContentCard>
        {path === "/login" && (
          <LoginForm handleChange={handleChange} handleSubmit={handleSubmit} />
        )}
        {path === "/register" && (
          <RegistrationForm
            handleChange={handleChange}
            handleSubmit={handleSubmit}
          />
        )}
      </PublicContentCard>
    </PublicPageContainer>
  );
};

export default AuthPage;
