import React from "react";
import { Container, Grid, TextField } from "@material-ui/core";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import MainButton from "../../../Shared/MainButton/MainButton";
import { useStyles } from "./RegistrationFormStyles";

type FormProps = {
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
};

const RegistrationForm: React.FC<FormProps> = ({
  handleChange,
  handleSubmit,
}: FormProps) => {
  const styles = useStyles();
  const theme = createMuiTheme({
    palette: {
      secondary: {
        main: "#1b6d34",
      },
    },
  });
  return (
    <Container>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <ThemeProvider theme={theme}>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                onChange={handleChange}
                fullWidth
                required
                label="Email"
                name="email"
                className={styles.margin}
                color="secondary"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                onChange={handleChange}
                fullWidth
                required
                label="Password"
                name="password"
                type="password"
                className={styles.margin}
                color="secondary"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                onChange={handleChange}
                fullWidth
                required
                label="Confirm password"
                name="password2"
                type="password2"
                className={styles.margin}
                color="secondary"
              />
            </Grid>
          </Grid>
        </ThemeProvider>
        <Grid container spacing={3} className={styles.buttonsWrapper}>
          <Grid item xs={6}>
            <MainButton btnHandler="/login">Login</MainButton>
          </Grid>
          <Grid item xs={6}>
            <MainButton btnHandler="/register" type="submit">
              Register
            </MainButton>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
};

export default RegistrationForm;
