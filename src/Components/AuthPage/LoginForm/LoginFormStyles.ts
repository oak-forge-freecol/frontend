import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => ({
  margin: {
    margin: "10px 0",
  },
  buttonsWrapper: {
    marginTop: "20px",
  },
}));
