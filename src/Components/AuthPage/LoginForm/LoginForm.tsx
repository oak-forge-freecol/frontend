import React from "react";
import { Container, Grid, TextField } from "@material-ui/core";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import MainButton from "../../../Shared/MainButton/MainButton";
import { useStyles } from "./LoginFormStyles";

type FormProps = {
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
};

const LoginForm: React.FC<FormProps> = ({
  handleChange,
  handleSubmit,
}: FormProps) => {
  const styles = useStyles();
  const theme = createMuiTheme({
    palette: {
      secondary: {
        main: "#1b6d34",
      },
    },
  });
  return (
    <Container>
      <form
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
        data-testid="login-form"
      >
        <ThemeProvider theme={theme}>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                id="login-email"
                onChange={handleChange}
                fullWidth
                required
                label="Email"
                name="email"
                data-testid="email"
                className={styles.margin}
                color="secondary"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="login-password"
                onChange={handleChange}
                fullWidth
                required
                label="Password"
                name="password"
                type="password"
                data-testid="password"
                className={styles.margin}
                color="secondary"
              />
            </Grid>
          </Grid>
        </ThemeProvider>
        <Grid container spacing={3} className={styles.buttonsWrapper}>
          <Grid item xs={6}>
            <MainButton btnHandler="/register">Register</MainButton>
          </Grid>
          <Grid item xs={6}>
            <MainButton btnHandler="/home" type="submit">
              Login
            </MainButton>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
};

export default LoginForm;
