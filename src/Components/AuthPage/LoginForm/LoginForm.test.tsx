import React from "react";
import { render } from "@testing-library/react";
import LoginForm from "./LoginForm";

describe("LoginForm component", () => {
  const handleChange = () => console.log("handle change");
  const handleSubmit = () => console.log("handle submit");

  it("renders login form", () => {
    const { getByTestId } = render(
      <LoginForm handleChange={handleChange} handleSubmit={handleSubmit} />
    );
    expect(getByTestId("login-form")).toBeInTheDocument();
  });

  it("renders email, password inputs with labels", () => {
    const { getByTestId, getByLabelText } = render(
      <LoginForm handleChange={handleChange} handleSubmit={handleSubmit} />
    );
    expect(getByTestId("email")).toBeInTheDocument();
    expect(getByLabelText(/email/i)).toBeInTheDocument();
    expect(getByTestId("password")).toBeInTheDocument();
    expect(getByLabelText(/password/i)).toBeInTheDocument();
  });

  it("renders login, register buttons", () => {
    const { getByText } = render(
      <LoginForm handleChange={handleChange} handleSubmit={handleSubmit} />
    );
    expect(getByText("Login")).toBeInTheDocument();
    expect(getByText("Register")).toBeInTheDocument();
  });
});
