/* eslint-disable react/require-default-props */
import React from "react";
import classnames from "classnames";
import { Grid, CardMedia } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import freecol from "../../assets/oakForge-freecol/freecol2.png";
import { useStyles } from "./PublicContentCardStyles";

type ContentCardPublicProps = {
  children?: React.ReactNode;
  popup?: boolean;
};

const ContentCardPublic: React.FC<ContentCardPublicProps> = ({
  children,
  popup,
}: ContentCardPublicProps) => {
  const theme = useTheme();
  const styles = useStyles(theme);

  const rootStyles = classnames({
    [`${styles.root}`]: true,
    [`${styles.popup}`]: popup,
  });

  return (
    <Grid container className={rootStyles}>
      <Grid item xs={12}>
        <CardMedia image={freecol} className={styles.image} />
      </Grid>
      <Grid item xs={12}>
        {children}
      </Grid>
    </Grid>
  );
};

export default ContentCardPublic;
