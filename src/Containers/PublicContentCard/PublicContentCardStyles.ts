import { makeStyles } from "@material-ui/core/styles";
import bgPaper from "../../assets/oakForge-freecol/bgPaper.png";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    flexWrap: "wrap",
    width: "300px",
    border: "1px solid #000",
    padding: "20px",
    marginTop: "30px",
    backgroundImage: `url(${bgPaper})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top",
    backgroundSize: "cover",
    [theme.breakpoints.up("sm")]: {
      width: "350px",
    },
  },
  popup: {
    marginTop: "0",
  },
  image: {
    width: "250px",
    height: "80px",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    [theme.breakpoints.up("sm")]: {
      width: "300px",
    },
  },
}));
