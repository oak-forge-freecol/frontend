import React, { useState } from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { Grid, CardMedia, Typography, Hidden } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import cx from "classnames";
import { useStyles } from "./PrivatePageContainerStyles";
import logo from "../../assets/oakForge-freecol/galleon.png";
import DesktopMenu from "./DesktopMenu/DesktopMenu";
import TabletMenu from "./TabletMenu/TabletMenu";
import MobileMenu from "./MobileMenu/MobileMenu";
import CornerButton from "../../Shared/CornerButton/CornerButton";

enum ButtonPosition {
  top,
  bottom,
}

type Props = {
  children: JSX.Element;
};

const PrivatePageContainer: React.FC<Props> = ({ children }: Props) => {
  const [open, setOpen] = useState<boolean>(false);
  const theme = useTheme();
  const styles = useStyles(theme);
  const rootStyles = cx({
    [`${styles.root}`]: true,
    [`${styles.open}`]: open,
  });
  const mobile = useMediaQuery(theme.breakpoints.up("xs"));
  const tablet = useMediaQuery(theme.breakpoints.up("sm"));
  const desktop = useMediaQuery(theme.breakpoints.up("md"));
  return (
    <Grid container className={rootStyles}>
      <Grid item sm={12} className={styles.playButton}>
        <Hidden only={["xs"]}>
          <CornerButton position={ButtonPosition.top}>Play</CornerButton>
        </Hidden>
      </Grid>
      <Grid item xs={12} className={styles.profileButton}>
        <Hidden smUp>
          <CornerButton position={ButtonPosition.bottom}>Profile</CornerButton>
        </Hidden>
      </Grid>
      <Grid item xs={12} className={styles.logo}>
        <CardMedia image={logo} className={styles.logoImage} />
        <div className={styles.logoTitle}>
          <Typography component="h1">FreeCol</Typography>
        </div>
      </Grid>
      <Grid item xs={12}>
        {children}
      </Grid>
      {mobile && !tablet && !desktop && (
        <MobileMenu open={open} setOpen={setOpen} />
      )}
      {tablet && !desktop && <TabletMenu open={open} setOpen={setOpen} />}
      {desktop && <DesktopMenu />}
    </Grid>
  );
};

export default PrivatePageContainer;
