import { makeStyles } from "@material-ui/core/styles";
import compass from "../../../assets/oakForge-freecol/compass.png";

export const useStyles = makeStyles(() => ({
  root: {
    position: "fixed",
    top: "0",
    width: "100vw",
    minHeight: "100vh",
    backgroundColor: "rgba(195,193,158,0.9)",
    transform: "translateX(-100%)",
  },
  menu: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    height: "100%",
    top: "0",
    padding: "80px 0 0 0",

    "& > a": {
      textDecoration: "none",
      padding: "10px 0",
      fontSize: "20px",
      color: "#000",
    },
  },
  compass: {
    width: "70px",
    height: "70px",
    backgroundImage: `url(${compass})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "right",
    backgroundSize: "contain",
    position: "absolute",
    left: "50%",
    transform: "translateX(-50%)",
    bottom: "50px",
  },
  open: {
    transform: "translateX(0)",
    overflow: "hidden",
  },
}));
