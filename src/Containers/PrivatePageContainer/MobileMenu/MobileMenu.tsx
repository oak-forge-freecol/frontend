import React from "react";
import cx from "classnames";
import { Grid, Box, Link } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { useStyles } from "./MobileMenuStyles";
import Burger from "../../../Shared/Burger/Burger";

interface MenuProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const MobileMenu: React.FC<MenuProps> = ({ open, setOpen }: MenuProps) => {
  const theme = useTheme();
  const styles = useStyles(theme);
  const rootStyles = cx({
    [`${styles.root}`]: true,
    [`${styles.open}`]: open,
  });
  return (
    <>
      <Burger open={open} setOpen={setOpen} />
      <Grid
        container
        spacing={1}
        direction-xs-column
        justify="center"
        alignItems="center"
        className={rootStyles}
      >
        <Grid item xs={12}>
          <Box className={styles.menu}>
            <Link href="/home">Home</Link>
            <Link href="/home">About game</Link>
            <Link href="/home">Best Scores</Link>
            <Link href="/home">Profile</Link>
            <Link href="/home">Rules</Link>
            <Link href="/home">Logout</Link>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <div className={styles.compass} />
        </Grid>
      </Grid>
    </>
  );
};

export default MobileMenu;
