import { makeStyles } from "@material-ui/core/styles";
import compass from "../../../assets/oakForge-freecol/compass.png";

export const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "100px 0 50px 0",
    position: "absolute",
    left: "0",
    width: "150px",
    height: "100%",
    backgroundColor: "rgba(195,193,158,0.7)",
    transform: "translateX(-100%)",
  },
  buttons: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    "& > a": {
      margin: "20px 0",
    },
    "&  a:last-of-type": {
      alignSelf: "flex-end",
      margin: "auto 0 0 0",
    },
  },
  compass: {
    width: "70px",
    height: "70px",
    backgroundImage: `url(${compass})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "right",
    backgroundSize: "contain",
    marginTop: "30px",
  },
  open: {
    transform: "translateX(0)",
  },
}));
