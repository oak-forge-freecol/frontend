import React from "react";
import cx from "classnames";
import Box from "@material-ui/core/Box";
import { useTheme } from "@material-ui/core/styles";
import { useStyles } from "./TabletMenuStyles";
import MainButton from "../../../Shared/MainButton/MainButton";
import Burger from "../../../Shared/Burger/Burger";

interface MenuProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const TabletMenu: React.FC<MenuProps> = ({ open, setOpen }: MenuProps) => {
  const theme = useTheme();
  const styles = useStyles(theme);
  const rootStyles = cx({
    [`${styles.root}`]: true,
    [`${styles.open}`]: open,
  });
  return (
    <>
      <Burger open={open} setOpen={setOpen} />
      <div className={rootStyles}>
        <Box className={styles.buttons}>
          <MainButton btnHandler="/home" secondary>
            Home
          </MainButton>
          <MainButton btnHandler="/home" secondary>
            About game
          </MainButton>
          <MainButton btnHandler="/home" secondary>
            Best Scores
          </MainButton>
          <MainButton btnHandler="/home" secondary>
            Profile
          </MainButton>
          <MainButton btnHandler="/home" secondary>
            Rules
          </MainButton>
          <MainButton btnHandler="/login" secondary>
            Logout
          </MainButton>
        </Box>
        <div className={styles.compass} />
      </div>
    </>
  );
};

export default TabletMenu;
