import { makeStyles } from "@material-ui/core/styles";
import bgImage from "../../assets/oakForge-freecol/bgImage.jpg";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
    minHeight: "100vh",
    backgroundImage: `linear-gradient(rgba(255,255,255,0.7),rgba(255,255,255,0.7)),url(${bgImage})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top",
    backgroundSize: "cover",
    paddingBottom: "100px",
    "& > a": {
      display: "none",
      [theme.breakpoints.up("sm")]: {
        display: "flex",
        position: "absolute",
        left: "30px",
        top: "30px",
      },
    },
  },
  playButton: {
    width: "200px",
    height: "100px",
    position: "fixed",
    top: "0",
    right: "0",
    cursor: "pointer",
  },
  profileButton: {
    width: "200px",
    height: "100px",
    position: "fixed",
    bottom: "0",
    left: "0",
    cursor: "pointer",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  logo: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: "20px",
    heigth: "100px",
    width: "100vw",
    backgroundColor: "rgba(255,255,255,0.7)",
    borderBottom: "1px solid #C3C19E",

    [theme.breakpoints.up("sm")]: {
      justifyContent: "center",
      heigth: "120px",
      width: "100%",
      backgroundColor: "transparent",
      border: "none",
    },
  },
  logoTitle: {
    alignSelf: "center",
    height: "100px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    "& > h1": {
      fontSize: "32px",
      fontFamily: "Felipa",
      marginTop: "10px",
      letterSpacing: "0.8px",
      [theme.breakpoints.up("sm")]: {
        fontSize: "64px",
        marginTop: "20px",
        transform: "translateX(-10px)",
      },
    },
    [theme.breakpoints.up("sm")]: {
      height: "120px",
      display: "block",
    },
  },

  logoImage: {
    width: "80px",
    height: "80px",
    marginTop: "5px",
    backgroundSize: "contain",
    [theme.breakpoints.up("sm")]: {
      width: "120px",
      height: "120px",
      marginTop: "20px",
    },
  },
  open: {
    overflow: "hidden",
  },
}));
