import React from "react";
import { useTheme } from "@material-ui/core/styles";
import { useStyles } from "./DesktopMenuStyles";
import MainButton from "../../../Shared/MainButton/MainButton";

const DesktopMenu: React.FC = () => {
  const theme = useTheme();
  const styles = useStyles(theme);
  return (
    <div className={styles.root}>
      <div className={styles.compass} />
      <MainButton btnHandler="/home" secondary>
        Home
      </MainButton>
      <MainButton btnHandler="/home" secondary>
        About game
      </MainButton>
      <MainButton btnHandler="/home" secondary>
        Best Scores
      </MainButton>
      <MainButton btnHandler="/home" secondary>
        Profile
      </MainButton>
      <MainButton btnHandler="/home" secondary>
        Rules
      </MainButton>
      <MainButton btnHandler="/login" secondary>
        Logout
      </MainButton>
      <div className={styles.compass} />
    </div>
  );
};

export default DesktopMenu;
