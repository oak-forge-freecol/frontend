import { makeStyles } from "@material-ui/core/styles";
import compass from "../../../assets/oakForge-freecol/compass.png";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    position: "fixed",
    bottom: "0",
    width: "100vw",
    height: "60px",
    backgroundColor: theme.palette.primary.light,
    [theme.breakpoints.up("lg")]: {
      height: "80px",
    },
  },
  compass: {
    width: "50px",
    height: "50px",
    backgroundImage: `url(${compass})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "right",
    backgroundSize: "contain",
    [theme.breakpoints.up("lg")]: {
      width: "60px",
      height: "60px",
    },
  },
}));
