import React from "react";
import { CardMedia, Grid } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { useLocation } from "react-router-dom";
import { useStyles } from "./PublicPageContainerStyles";
import logo from "../../assets/oakForge-freecol/galleon.png";
import MainButton from "../../Shared/MainButton/MainButton";

type Props = {
  children: JSX.Element;
};

const PublicPageContainer: React.FC<Props> = ({ children }: Props) => {
  const theme = useTheme();
  const styles = useStyles(theme);
  const location = useLocation();
  const path = location.pathname;
  return (
    <Grid container className={styles.root}>
      {path === "/" && (
        <Grid item xs={12}>
          <div className={styles.loginButton}>
            <MainButton btnHandler="/login">Login</MainButton>
          </div>
        </Grid>
      )}
      <Grid item xs={12}>
        <CardMedia image={logo} className={styles.logo} />
      </Grid>
      <Grid item xs={12}>
        {children}
      </Grid>
    </Grid>
  );
};

export default PublicPageContainer;
