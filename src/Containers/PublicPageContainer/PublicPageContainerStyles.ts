import { makeStyles } from "@material-ui/core/styles";
import bgImage from "../../assets/oakForge-freecol/bgImage.jpg";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100vw",
    minHeight: "100vh",
    backgroundImage: `linear-gradient(rgba(255,255,255,0.4),rgba(255,255,255,0.4)),url(${bgImage})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top",
    backgroundSize: "cover",
    paddingBottom: "20px",
  },
  loginButton: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex",
      position: "absolute",
      left: "30px",
      top: "30px",
    },
  },
  logo: {
    width: "150px",
    height: "150px",
    marginTop: "20px",
    [theme.breakpoints.up("sm")]: {
      widht: "180px",
      height: "180px",
      marginTop: "150px",
    },
    [theme.breakpoints.up("md")]: {
      marginTop: "70px",
    },
  },
}));
