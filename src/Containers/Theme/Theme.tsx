import React from "react";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import theme from "./ThemeStyles";

type Props = {
  children: JSX.Element;
};

const Theme: React.FC<Props> = ({ children }: Props) => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    {children}
  </ThemeProvider>
);

export default Theme;
