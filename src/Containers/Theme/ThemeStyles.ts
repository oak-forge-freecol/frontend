import { createMuiTheme } from "@material-ui/core";
// const fontPrimary = "'Shadowed Black', sans-serif";
const fontSecondary = "'ElMessiri', sans-serif";

const theme = createMuiTheme({
  typography: {
    fontFamily: fontSecondary,
  },
  palette: {
    primary: {
      light: "rgba(195,193,158,0.7)",
      main: "rgb(195,193,158)",
      dark: "rgb(175,173,142)",
      contrastText: "#000",
    },
    secondary: {
      light: "rgba(244,232,206,0.7)",
      main: "rgb(244,232,206)",
      dark: "rgb(219,208,185)",
      contrastText: "#000",
    },
  },
});

theme.props = {
  MuiInput: {
    disableUnderline: true,
  },
};

export default theme;
