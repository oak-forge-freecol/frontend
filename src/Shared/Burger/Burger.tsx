import React from "react";
import cx from "classnames";
import { useTheme } from "@material-ui/core/styles";
import { useStyles } from "./ButgerStyles";

interface BurgerProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

const Burger: React.FC<BurgerProps> = ({ open, setOpen }: BurgerProps) => {
  const theme = useTheme();
  const styles = useStyles(theme);
  const burgerStyles = cx({
    [`${styles.burgerButton}`]: true,
    [`${styles.open}`]: open,
  });
  return (
    // eslint-disable-next-line react/button-has-type
    <button className={burgerStyles} onClick={() => setOpen(!open)}>
      <div />
      <div />
      <div />
    </button>
  );
};

export default Burger;
