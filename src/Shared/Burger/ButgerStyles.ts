import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  burgerButton: {
    position: "absolute",
    top: "50px",
    right: "20px",
    transform: "translate(-50%,-50%)",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    width: "2rem",
    height: "2rem",
    background: "transparent",
    border: "none",
    cursor: "pointer",
    padding: "0",
    zIndex: 10,
    [theme.breakpoints.up("sm")]: {
      left: "75px",
    },
    "&:focus": {
      outline: "none",
    },
    "& > div": {
      width: "2rem",
      height: "0.25rem",
      background: "#000",
      borderRadius: "10px",
      transition: "all 0.3s linear",
      position: "relative",
      transformOrigin: "1px",
    },
  },
  open: {
    "& > div:first-child": {
      transform: "rotate(45deg)",
    },
    "& > div:nth-child(2)": {
      opacity: "0",
      transform: "translateX(20px)",
    },

    "& > div:nth-child(3)": {
      transform: "rotate(-45deg)",
    },
  },
}));
