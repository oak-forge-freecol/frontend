import React from "react";
import cx from "classnames";
import { Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { useStyles } from "./CornerButtonStyles";

enum ButtonPosition {
  top,
  bottom,
}

type ButtonProps = {
  children: React.ReactNode;
  position: ButtonPosition;
};

const CornerButton: React.FC<ButtonProps> = ({
  children,
  position,
}: ButtonProps) => {
  const theme = useTheme();
  const styles = useStyles(theme);

  const buttonStyles = cx({
    [`${styles.playButtonBg}`]: true,
    [`${styles.top}`]: position === 0,
    [`${styles.bottom}`]: position === 1,
  });

  return (
    <div className={buttonStyles}>
      <Typography component="h2">{children}</Typography>
    </div>
  );
};

export default CornerButton;
