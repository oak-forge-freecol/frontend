import { makeStyles } from "@material-ui/core/styles";
import playBg from "../../assets/oakForge-freecol/playBg.png";

export const useStyles = makeStyles(() => ({
  playButtonBg: {
    width: "100%",
    height: "100%",
    position: "relative",
    backgroundImage: `url(${playBg})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "right",
    backgroundSize: "contain",
    "& > h2": {
      fontSize: "24px",
      position: "absolute",
    },
  },
  top: {
    transform: "scaleY(-1)",
    "& > h2": {
      top: "40%",
      right: "20%",
      transform: "scaleY(-1)",
    },
  },
  bottom: {
    transform: "scaleX(-1)",
    "& > h2": {
      bottom: "20%",
      left: "50%",
      transform: "scaleX(-1)",
    },
  },
}));
