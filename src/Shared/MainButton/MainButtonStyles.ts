import { makeStyles } from "@material-ui/core/styles";
import bgPaper from "../../assets/oakForge-freecol/bgPaper.png";

export const useStyles = makeStyles((theme) => ({
  button: {
    border: `1px solid ${theme.palette.grey[900]}`,
    borderRadius: "0",
    width: "120px",
    height: "50px",
    boxShadow: theme.shadows[10],
    backgroundImage: `url(${bgPaper})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top",
    backgroundSize: "cover",
    "& > span": {
      textTransform: "capitalize",
      fontSize: "20px",
    },
    "&:hover": {
      backgroundColor: theme.palette.primary.light,
      boxShadow: theme.shadows[3],
    },
  },
  secondaryButton: {
    backgroundImage: "none",
    backgroundColor: "transparent",
    "& > span": {
      textAlign: "center",
      lineHeight: "100%",
    },
  },

  wideButton: {
    width: "120px",
    [theme.breakpoints.up("sm")]: {
      width: "240px",
    },
  },
}));
