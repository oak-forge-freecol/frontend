import React from "react";
import { Button } from "@material-ui/core";
import classnames from "classnames";
import { useStyles } from "./MainButtonStyles";

interface ButtonProps {
  children: React.ReactNode;
  btnHandler: (() => void) | string;
  secondary?: boolean;
  wide?: boolean;
  type?: string;
}

const MainButton: React.FC<ButtonProps> = ({
  children,
  btnHandler,
  secondary,
  wide,
  type,
}: ButtonProps) => {
  const styles = useStyles();

  const buttonStyles = classnames({
    [`${styles.button}`]: true,
    [`${styles.wideButton}`]: wide,
    [`${styles.secondaryButton}`]: secondary,
  });

  if (typeof btnHandler === "string") {
    return (
      <Button href={btnHandler} className={buttonStyles} type={type}>
        {children}
      </Button>
    );
  }
  if (typeof btnHandler === "function") {
    return (
      <Button onClick={btnHandler} className={buttonStyles}>
        {children}
      </Button>
    );
  }
  return <Button />;
};

MainButton.defaultProps = {
  secondary: false,
  wide: false,
  type: "/",
};

export default MainButton;
